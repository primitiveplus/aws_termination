This script goes through a list, termlist.txt, of instance ids. It then stops the instances if running, turns off API termination protection, and terminates them.
