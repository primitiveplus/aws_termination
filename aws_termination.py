#!/usr/bin/python

import boto3, sys, pprint

#for later, to allow CMD arguments
#import argv

#only dealing with ec2 here
ec2 = boto3.resource('ec2')

#grab the list of servers to be terminated and turn into parseable input
file = open('termlist.txt','r')
file_mod = file.read().splitlines()

#create dict of instances from the file
instance_list = []
for id_file in file_mod:
	instance = ec2.Instance(id=id_file)
	instance_list.append(instance)

#check to see if the instance is stopped, if not, stop
print "checking to see that each instance is stopped..."
for instance in instance_list:
#	try:
        for single_tag in instance.tags:
        	if single_tag['Key'] == 'Name':
	                name_tag = single_tag['Value']

	if instance.state['Code'] == 16:
		print "%s is running. Stop it now?." % name_tag
		ask = raw_input("Type y or n: ")
		if ask == 'y':
			instance.stop()
		elif ask == 'n': 
			print "OK. Remove it from the termination task, then?"
			ask2 == raw_input("Type y or n: ")
			if ask2 == 'y':
				instance_list.remove(instance)
			else:
				sys.exit("kk. rethink your list and come back pls")
	elif instance.state['Code'] == 80:
		pass
	else:
		print "%r has state %r." % (name_tag, instance.state)
		sys.exit("Do something about that pls")
#	except Exemption:
#		pass

#check for termination protection, turn off
print "checking for termination protection...."
for instance in instance_list:
	for single_tag in instance.tags:
	        if single_tag['Key'] == 'Name':
	                name_tag = single_tag['Value']
	attrs = instance.describe_attribute(DryRun=False, Attribute='disableApiTermination')	
	if attrs['DisableApiTermination']['Value'] == True:
		print "%s has termination protection enabled. Turn off?" % name_tag
		ask = raw_input("Type y or n >  ")
		if ask == 'y':
			instance.modify_attribute(DryRun=False, Attribute='disableApiTermination', Value='False')
		elif ask == 'n':
			print "OK. Remove from the termination task, then?"
			ask3 = raw_input("Type y or n: ")
			if ask3 == 'y':	
				instance_list.remove(instance)
			else:
				sys.exit("kk. redo the list and come back pls.")
		else:
			sys.exit("Unsure about input. Exiting...")
	
#terminate the instances
print "Okay, time to terminate! Type OKAY to continue; CHECK to review the list of servers to be terminated, otherwise CTRL-C the heck out of here now!!"
prompt = raw_input("> ")

if prompt == 'OKAY':
	for instance in instance_list:
		print "Terminating %s" % instance.id
		instance.terminate()
	print "Done!"
elif prompt == 'CHECK':
	print "one sec..."
	pretty_list = []
	for instance in instance_list:
		for single_tag in instance.tags:
			if single_tag['Key'] == 'Name':
				name_tag = single_tag['Value']
		instance_info = {'id' : instance.id, 'Name' : name_tag}
		pretty_list.append(instance_info)
	pprint.pprint(pretty_list)
	print '''
	Continue with the termination?
	'''
	prompt2 = raw_input("Press Enter to Continue, otherwise CTRL-C ")
	for instance in instance_list:
                print "Terminating %s" % instance.id
                instance.terminate()
        print "Done!"
else:
	sys.exit("o, kay.")
